package com.course.controller;

import com.course.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Log4j
@RestController
@Api(value = "v1", description = "首个Demo版本")
@RequestMapping("v1")
public class Demo {

    //获取执行sql语句对象
    @Autowired
    private SqlSessionTemplate template;

    //查询用户总数
    @RequestMapping(value = "/getUserCount", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户总数", httpMethod = "GET")
    public int getUserCount(){
        return template.selectOne("getUserCount");
    }

    //新增用户(携带信息过来用POST)
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ApiOperation(value = "新增用户", httpMethod = "POST")
    public int addUser(@RequestBody User user){
        return template.insert("addUser", user);
    }

    //更新用户
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ApiOperation(value = "更新用户", httpMethod = "POST")
    public int updateUser(@RequestBody User user){
        return template.update("updateUser", user);
    }

    //删除用户
    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    @ApiOperation(value = "更新用户", httpMethod = "GET")
    public int deleteUser(@RequestParam int id){
        return template.delete("deleteUser", id);
    }


}
