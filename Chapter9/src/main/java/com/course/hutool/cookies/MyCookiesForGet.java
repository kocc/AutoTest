package com.course.hutool.cookies;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.setting.dialect.Props;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.List;

/**
 * @author zlm
 * @create 2022-12-20 13:26    /**
 * @ClassName MyCookiesForGet
 * @Description TODO
 * @Author HP
 * @Date 2022-12-20 13:26
 * @Version 1.0
 **/
public class MyCookiesForGet {

    private String url;
    private String getCookies;
    private String getWithCookies;
    private CookieStore store;

    @BeforeTest
    public void beforeTest(){
        //application.properties
        Props props = new Props("application.properties");
        url = props.getProperty("test.url");
        getCookies = props.getProperty("getCookies.uri");
        getWithCookies = props.getProperty("test.get.with.cookies");

    }

    @Test
    public void testGetCookies(){
        String testCookiesUrl = url + getCookies;
        System.out.println(testCookiesUrl);
        HttpResponse response = HttpRequest.get(testCookiesUrl)
                .timeout(5000)
                .execute();
        String body = response.body();
        System.out.println(body);
        List<HttpCookie> cookies = response.getCookies();
        this.store = (CookieStore) cookies;      //抽取出来为下一步进行使用
        for (HttpCookie cookie : cookies) {
            String name = cookie.getName();
            String value = cookie.getValue();
            System.out.println(name + ": " + value);
        }
    }

    @Test(dependsOnMethods = {"testGetCookies"})
    public void testGetWithCookies(){
        String testGetWithCookiesUrl = url + getWithCookies;
        System.out.println(testGetWithCookiesUrl);
        HttpResponse response = HttpRequest.get(url)
                .cookie(String.valueOf(store))
                .timeout(5000)
                .execute();
        int status = response.getStatus();
        String body = response.body();
        System.out.println(status);
        System.out.println(body);
    }



}
