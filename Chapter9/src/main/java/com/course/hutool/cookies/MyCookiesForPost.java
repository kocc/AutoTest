package com.course.hutool.cookies;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import cn.hutool.setting.dialect.Props;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.List;

/**
 * @author zlm
 * @create 2022-12-21 15:20
 * @ClassName MyCookiesForPost
 * @Description TODO
 * @Author HP
 * @Date 2022-12-21 15:20
 * @Version 1.0
 **/
public class MyCookiesForPost {
    private String url;
    private String getCookies;
    private String getWithCookies;
    private String postWithCookies;
    private CookieStore store;

    @BeforeTest
    public void beforeTest(){
        //application.properties
        Props props = new Props("application.properties");
        url = props.getProperty("test.url");
        getCookies = props.getProperty("getCookies.uri");
        getWithCookies = props.getProperty("test.get.with.cookies");
        postWithCookies = props.getProperty("test.post.with.cookies");

    }

    @Test
    public void testGetCookies(){
        String uri = url + getCookies;
        System.out.println(uri);
        HttpResponse response = HttpRequest.get(uri)
                .timeout(5000)
                .execute();
        String body = response.body();
        System.out.println(body);
        List<HttpCookie> cookies = response.getCookies();
        this.store = (CookieStore) cookies;      //抽取出来为下一步进行使用
        for (HttpCookie cookie : cookies) {
            String name = cookie.getName();
            String value = cookie.getValue();
            System.out.println(name + ": " + value);
        }
    }

    @Test(dependsOnMethods = {"testGetCookies"})
    public void testPostCookies() {
        String uri = url + postWithCookies;
        System.out.println(uri);
        //设置参数
        JSONObject param = new JSONObject();
        param.set("name","Anna");
        param.set("age","28");
        //发送请求
        HttpResponse response = HttpRequest.post(uri)
                .header("content-type", "application/json")
                .timeout(5000)
                .form(param)
                .execute();
        //查看相应
        String body = response.body();
        JSONObject resultJson = new JSONObject(body);
        Object success = resultJson.getJSONObject("success");
        Object status = resultJson.getJSONObject("status");
        Assert.assertEquals("success",success);
        Assert.assertEquals("1",status);

    }



}
