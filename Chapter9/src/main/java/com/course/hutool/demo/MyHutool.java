package com.course.hutool.demo;

import cn.hutool.http.HttpRequest;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * @author zlm
 * @create 2022-12-19 16:00
 * @ClassName MyHutool
 * @Description Hutool工具类的使用
 * @Author HP
 * @Date 2022-12-19 16:00
 * @Version 1.0
 **/
public class MyHutool {

    /**
     * @description 使用原版HttpClient
     */
    @Test
    public void test() throws IOException {

        String url = "https://www.baidu.com/";
        String result;
        HttpGet get = new HttpGet(url);
        DefaultHttpClient client = new DefaultHttpClient();
        CloseableHttpResponse response = client.execute(get);
        result = EntityUtils.toString(response.getEntity(),"utf-8");
        System.out.println(result);
    }

    /**
     * @description 使用Hutool工具类进行重构，返回结果没有本质区别
     */
    @Test
    public void test1(){

        String url = "https://www.baidu.com/";

        String result = HttpRequest.get(url)
                //.header(Header.USER_AGENT, "Hutool http")//头信息，多个头信息多次调用此方法即可
                //.form(paramMap)//表单内容
                .timeout(20000)//超时，毫秒
                .execute().body();
        System.out.println(result);
    }

}