package com.course.httpclient.cookies;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class MyCookiesForPost {

    public class MyCookiesForGet {

        private String url;
        private ResourceBundle bundle;
        //用于存储cookies信息的变量
        private CookieStore store;

        @BeforeTest
        public void beforeTest(){
            //自动识别resources下的资源文件(无需写后缀文件名)
            bundle = ResourceBundle.getBundle("application", Locale.CHINA);
            url = bundle.getString("test.url");
        }

        @Test
        public void testGetCookies() throws IOException {
            String result;
            String uri = bundle.getString("getCookies.uri");
            //从配置文件拼接完整的地址url
            String testUrl = this.url + uri;
            HttpGet get = new HttpGet(testUrl);

            //测试逻辑代码书写
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(get);
            result = EntityUtils.toString(response.getEntity(),"utf-8");
            System.out.println(result);

            //获取cookies信息
            this.store = client.getCookieStore();
            List<Cookie> cookieList = store.getCookies();
            for (Cookie cookie: cookieList) {
                String name = cookie.getName();
                String value = cookie.getValue();
                System.out.println("cookie name : " + name + " , cookie value : " + value);
            }
        }

        //带上cookies信息访问其他接口(依赖测试)
        @Test(dependsOnMethods = {"testGetCookies"})
        public void testPostMethod() throws IOException {
            String uri = bundle.getString("test.post.with.cookies");
            //拼接最终的测试地址
            String testUrl = this.url + uri;

            //声明一个Client兑现，用于进行方法的执行
            DefaultHttpClient client = new DefaultHttpClient();

            //声明一个post方法
            HttpPost post = new HttpPost(testUrl);

            //添加参数(json格式)
            JSONObject param = new JSONObject();
            param.put("name","Cathy");
            param.put("age","20");

            //把参数信息添加到方法中
            StringEntity entity = new StringEntity(param.toString(),"utf-8");
            post.setEntity(entity);

            //设置请求头信息
            post.setHeader("content-type","application/json");

            //声明一个对象来进行响应结果的存储
            String result;

            //设置cookies信息
            client.setCookieStore(this.store);

            //执行post方法
            HttpResponse response = client.execute(post);

            //获取响应结果
            result = EntityUtils.toString(response.getEntity(),"utf-8");
            System.out.println(result);

            //处理结果，即判断返回结果是否符合预期(json格式的字符串返回结果需要转换成json对象)
            JSONObject resultJson = new JSONObject(result);
            String success = (String) resultJson.get("name");
            String status = (String) resultJson.get("status");
            Assert.assertEquals("success",success);
            Assert.assertEquals("1",status);
        }
    }

}
