package com.course.server;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zlm
 * @create 2022-12-22 22:56    /**
 * @ClassName SampleController
 * @Description TODO
 * @Author HP
 * @Date 2022-12-22 22:56
 * @Version 1.0
 **/
@RestController
public class HelloController {

    @RequestMapping(value = "/hello",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String hello(){
        return "Hello World";
    }

    @RequestMapping(value = "/list",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String list(){
        List<String> resultList = new ArrayList<>();
        resultList.add("Anna");
        resultList.add("Betty");
        return resultList.toString();
    }

}
