package com.course.server;

import com.course.bean.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Api(value = "/",description = "这是全部post请求")
@RequestMapping("/v1")
public class MyPostServer {

    //用于存放cookie信息
    private static Cookie cookie;

    //用户登陆成功获取到cookie。再访问其他接口获取商品列表
    @RequestMapping(value = "login",method = RequestMethod.POST)
    @ApiOperation(value = "登陆接口",httpMethod = "POST")
    public String Login(HttpServletResponse response,
                        @RequestParam(value = "userName", required = true) String userName,
                        @RequestParam(value = "password", required = true) String password){
        if (userName.equals("Anna") && password.equals("123456")){
            cookie = new Cookie("login", "true");
            response.addCookie(cookie);
            return "登录成功";
        }
        return "用户名或密码错误";
    }

    @RequestMapping(value = "/getUserList", method = RequestMethod.POST)
    @ApiOperation(value = "获取用户列表", httpMethod = "POST")
    public String getUserList(HttpServletRequest request,
                            @RequestBody User u){
        //获取并验证cookie
        User user;
        Cookie[] cookies = request.getCookies();
        for (Cookie c : cookies) {
            if (c.getName().equals("login")
                    && c.getValue().equals("true")
                    && u.getUserName().equals("Anna")
                    && u.getPassword().equals("123456")

            ){
                user = new User();
                user.setName("Betty");
                user.setAge("22");
                user.setSex("woman");
                return user.toString();
            }
        }
        return "参数不合法";
    }


}
