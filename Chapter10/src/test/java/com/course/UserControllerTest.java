package com.course;

import io.swagger.annotations.ApiOperation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.equalTo;
/**
 * @author zlm
 * @create 2022-12-22 16:37
 * @ClassName UserControllerTest
 * @Description 对脚手架进行单元测试
 * @Author HP
 * @Date 2022-12-22 16:37
 * @Version 1.0
 **/
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("验证返回值和cookies信息的正确")
    @ApiOperation(value = "通过该方法获取到cookies", httpMethod = "GET")
    public void testMyGetServer() throws Exception {
        //设置cookies
        Cookie cookies = new Cookie("login","true");
        cookies.setPath("/");

        mockMvc.perform(get("/getCookies")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("恭喜成功获得cookies信息")));

        Cookie cookiesGet = mockMvc.perform(get("/getCookies")
                        .cookie(cookies)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                        .andReturn()
                        //MockHttpServletResponse实现了HttpServletResponse并添加了获取cookies的方法
                        .getResponse()
                        //一般获取cookies方法是通过HttpServletRequest的request.getCookies()，单元测试中是例外通过response获取;
                        .getCookies()[0];
        String name = cookiesGet.getName();
        String value = cookiesGet.getValue();
        assertThat(name, equalTo("login"));
        assertThat(value, equalTo("true"));

    }

    @Test
    @DisplayName("验证必须携带cookies才能访问")
    @ApiOperation(value = "要求客户端携带cookies才能访问的get请求" ,httpMethod = "GET")
    public void testWithCookies() throws Exception {

        mockMvc.perform(get("/get/with/cookies")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("必须携带cookies信息")));

        String content = mockMvc.perform(get("/get/with/cookies")
                        //.cookie(cookieu)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertThat(content, equalTo("必须携带cookies信息"));

    }



}
