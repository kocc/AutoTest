package com.course;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author zlm
 * @create 2022-12-24 19:55
 * @ClassName HelloTest
 * @Description 验证返回值的文本信息是否支持List等
 * @Author HP
 * @Date 2022-12-24 19:55
 * @Version 1.0
 **/
@SpringBootTest
@AutoConfigureMockMvc
public class HelloControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("测试返回String")
    public void testHello() throws Exception {
        String result = "Hello World";
        String content = mockMvc.perform(get("/hello")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                                //.cookie()
                                )
                .andReturn()
                .getResponse().getContentAsString();
        assertThat(content,equalTo(result));

    }

    @Test
    @DisplayName("测试返回List<String>")
    public void testList() throws Exception {
        List<String> result = new ArrayList<>();
        result.add("Anna");result.add("Betty");

        String content = mockMvc.perform(get("/list")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        //.cookie()
                ).andReturn()
                .getResponse().getContentAsString();
        assertThat(content,equalTo(result.toString()));
    }

}
