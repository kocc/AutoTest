package com.course.testng;

import org.testng.annotations.Test;

public class IgnoreTest {

    @Test
    public void ignore1(){
        System.out.println("ignore1已执行");
    }

    //忽略测试，即不执行
    @Test(enabled = false)
    public void ignore2(){
        System.out.println("ignore2已执行");
    }

    //默认为true，等效于@Test
    @Test(enabled = true)
    public void ignore3(){
        System.out.println("ignore3已执行");
    }

}
