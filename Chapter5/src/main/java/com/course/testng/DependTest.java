package com.course.testng;

import org.testng.annotations.Test;

public class DependTest {

    @Test
    public void test1(){
        System.out.println("test1 run");
        //throw new RuntimeException();
    }

    //只运行test2时会先先要求所依赖的test1执行且成功，才能运行test2。否则会忽略。
    @Test(dependsOnMethods = "test1")
    public void test2(){
        System.out.println("test2 run");
    }

}
