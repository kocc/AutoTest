package com.course.testng.suite;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;

//定义测试套件
public class SuiteConfig {

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("beforeSuite已运行");
    }

    @AfterSuite
    public void afterSuite(){
        System.out.println("afterSuite已运行");
    }

    @BeforeSuite
    public void beforeTest(){
        System.out.println("beforeTest");
    }

    @AfterTest
    public void AfterTest(){
        System.out.println("afterTest");
    }


}
