package com.course.testng;

import org.testng.annotations.Test;

//期望异常测试
public class ExpectedException {

    //失败的异常测试(即期望为异常但实际成功)
    @Test(expectedExceptions = RuntimeException.class)
    public void runTimeExceptionFailed(){
        System.out.println("这是一个失败的异常测试");
    }

    //成功的异常测试(即期望为异常且实际异常)
    @Test(expectedExceptions = RuntimeException.class)
    public void runTimeExceptionSuccess(){
        System.out.println("这是成功的异常测试");
        throw new RuntimeException();
    }
}
