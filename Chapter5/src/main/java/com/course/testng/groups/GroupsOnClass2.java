package com.course.testng.groups;

import org.testng.annotations.Test;

@Test(groups = "stu")
public class GroupsOnClass2 {

    public void str1(){
        System.out.println("GroupsOnClass2中的方法stu1已运行");
    }

    public void str2(){
        System.out.println("GroupsOnClass2中的方法stu2已运行");
    }

}
